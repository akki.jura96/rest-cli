/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"fmt"
	"io"
	"net/http"
	"time"

	"github.com/spf13/cobra"
)

// timeCmd represents the time command
var timeCmd = &cobra.Command{
	Use:   "time",
	Short: "Get the time from the RESTful server",
	Long:  `This command mainly exists for making sure that the server works.`,
	Run: func(cmd *cobra.Command, args []string) {
		req, err := http.NewRequest(http.MethodGet, SERVER+PORT+"/time", nil)
		if err != nil {
			fmt.Println("TimeFunction - Error in req:", req)
			return
		}
		c := &http.Client{
			Timeout: 15 * time.Second,
		}
		resp, err := c.Do(req)
		if err != nil {
			fmt.Println(err)
			return
		}
		if resp == nil || (resp.StatusCode == http.StatusNotFound) {
			fmt.Println(resp)
			return
		}
		defer resp.Body.Close()
		data, _ := io.ReadAll(resp.Body)
		fmt.Println(string(data))
	},
}

func init() {
	rootCmd.AddCommand(timeCmd)
}
